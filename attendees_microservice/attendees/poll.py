import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    # monolith(is connected to settings.py when we renamed it)
    response = requests.get(url)
    # sending a get request to the url above
    content = json.loads(response.content)
    # turns json into a python string equal to content
    for conference in content["conferences"]:
        # loopping through the conferences from the json re
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
